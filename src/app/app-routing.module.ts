import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { ClientComponent } from './component/client/client.component';
import { StatusComponent } from './component/status/status.component';
import { UserComponent } from './component/user/user.component';
import { MyDataComponent } from './component/my-data/my-data.component';
import { HomeComponent } from './component/home/home.component';
import { ProviderComponent } from './component/provider/provider.component';
import { ReminderComponent } from './component/reminder/reminder.component';
import { ProductComponent } from './component/product/product.component';
import { SaleComponent } from './component/sale/sale.component';
import { ExpenseComponent } from './component/expense/expense.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'client', component: ClientComponent },
  { path: 'status', component: StatusComponent },
  { path: 'home', component: HomeComponent },
  { path: 'user', component: UserComponent },
  { path: 'my-data', component: MyDataComponent },
  { path: 'provider', component: ProviderComponent },
  { path: 'reminder', component: ReminderComponent },
  { path: 'product', component: ProductComponent },
  { path: 'sale', component: SaleComponent },
  { path: 'expense', component: ExpenseComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
