import { LoadingUiServices } from './services/loadingUi.services';
import { Component, OnInit } from '@angular/core';
import { NgBlockUI, BlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    LoadingUiServices.EventoBlockUI.subscribe((res) => {
      if (res) {
        this.blockShowUi();
      } else if (!res) {
        this.blockHideUI();
      }
    });
  }
  title: string = 'Gestão de Contas';
  logado: boolean = false;

  @BlockUI() blockUI: NgBlockUI;

  blockHideUI() {
    setTimeout(() => {
      this.blockUI.stop();
    }, 100);
  }

  blockShowUi() {
    this.blockUI.start();
  }
}
