import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { MatTableExporterModule } from 'mat-table-exporter';
import { BlockUIModule } from 'ng-block-ui';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientComponent } from './component/client/client.component';
import { ConfirmDialogComponent } from './component/confirm-dialog/confirm-dialog.component';
import { ExpenseComponent } from './component/expense/expense.component';
import { HeaderComponent } from './component/header/header.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { MenuComponent } from './component/menu/menu.component';
import { MyDataComponent } from './component/my-data/my-data.component';
import { ProductComponent } from './component/product/product.component';
import { ProviderComponent } from './component/provider/provider.component';
import { RegisterComponent } from './component/register/register.component';
import { ReminderComponent } from './component/reminder/reminder.component';
import { SaleComponent } from './component/sale/sale.component';
import { StatusComponent } from './component/status/status.component';
import { UnauthComponent } from './component/unauth/unauth.component';
import { UserComponent } from './component/user/user.component';
import { MaterialModule } from './modules/material/material.module';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    ClientComponent,
    StatusComponent,
    UserComponent,
    UnauthComponent,
    ConfirmDialogComponent,
    MyDataComponent,
    HomeComponent,
    ProviderComponent,
    ReminderComponent,
    ProductComponent,
    SaleComponent,
    ExpenseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SocialLoginModule,
    MatTableExporterModule,
    BlockUIModule.forRoot(),
    NgxChartsModule
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '46399956452-06arbo2unuj9kkaq7hf8kajauddil20s.apps.googleusercontent.com'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
