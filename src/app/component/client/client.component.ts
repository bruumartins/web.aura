import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from 'src/app/services/util.service';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { LoadingUiServices } from 'src/app/services/loadingUi.services';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../confirm-dialog/confirm-dialog.component'
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { ClientService } from '../../services/client.service';
import { Client } from '../../models/client';
import { CustomValidator } from '../../utils/customValidators';
import { Address } from 'src/app/models/address';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'name', 'document', 'phone', 'email', 'action'];
  dataSource = new MatTableDataSource<Client>();
  isLogged: boolean = false;
  openPost: boolean = false;
  openEdit: boolean = false;
  emailFormControl = new FormControl('', [
    Validators.email
  ]);
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  phoneFormControl = new FormControl('', []);
  documentFormControl = new FormControl('', [
    CustomValidator.isValidCpf()
  ]);
  countryFormControl = new FormControl('', []);
  stateFormControl = new FormControl('', []);
  cityFormControl = new FormControl('', []);
  cepFormControl = new FormControl('', []);
  neighborhoodFormControl = new FormControl('', []);
  streetFormControl = new FormControl('', []);
  numberFormControl = new FormControl('', []);
  matcher = new MyErrorStateMatcher();
  idClientUpdate: number = null;

  constructor(
    private clientService: ClientService,
    private utilService: UtilService,
    public dialog: MatDialog
  ) {
    this.isLogged = this.utilService.isLogged();
  }

  ngAfterViewInit() {
    this.get();
  }

  ngOnInit(): void { }

  get() {
    this.clientService.get().then((results: Array<Client>) => {
      this.dataSource = new MatTableDataSource<Client>(results);
      this.dataSource.paginator = this.paginator;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar cliente', 'ERRO');
    });
  }

  post() {
    if (this.nameFormControl.valid &&
      this.documentFormControl.valid &&
      this.phoneFormControl.valid &&
      this.emailFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let client: Client = {
        name: this.nameFormControl.value,
        document: this.documentFormControl.value,
        phone: this.phoneFormControl.value,
        email: this.emailFormControl.value,
        address: new Address()
      }

      if (this.countryFormControl.value) client.address.country = this.countryFormControl.value;
      if (this.stateFormControl.value) client.address.state = this.stateFormControl.value;
      if (this.cityFormControl.value) client.address.city = this.cityFormControl.value;
      if (this.cepFormControl.value) client.address.cep = this.cepFormControl.value;
      if (this.neighborhoodFormControl.value) client.address.neighborhood = this.neighborhoodFormControl.value;
      if (this.streetFormControl.value) client.address.street = this.streetFormControl.value;
      if (this.numberFormControl.value) client.address.number = this.numberFormControl.value;

      this.clientService.post(client).then(d => {
        this.resetForm();
        this.openPost = !this.openPost;
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Cliente inserido com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao inserir cliente', 'ERRO');
      });
    }
  }

  delete(item: any) {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.clientService.delete(item.id).then(d => {
      this.get();
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Cliente deletado com sucesso', 'SUCESSO');
    }).catch(err => {
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Erro ao deletar cliente', 'ERRO');
    });
  }

  openDialog(item: any) {
    const message = `Realmente deseja excluir o cliente? Essa ação não poderá ser desfeita.`;
    const dialogData = new ConfirmDialogModel("Confirme a exclusão", message);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.delete(item);
      }
    });
  }

  openFormUpdate(item: any) {
    this.clientService.getById(item.id).then((client: Client) => {
      this.nameFormControl.setValue(client.name);
      this.documentFormControl.setValue(client.document);
      this.emailFormControl.setValue(client.email);
      this.phoneFormControl.setValue(client.phone);
      if (client.address) {
        if (client.address.country) this.countryFormControl.setValue(client.address.country);
        if (client.address.state) this.stateFormControl.setValue(client.address.state);
        if (client.address.city) this.cityFormControl.setValue(client.address.city);
        if (client.address.cep) this.cepFormControl.setValue(client.address.cep);
        if (client.address.neighborhood) this.neighborhoodFormControl.setValue(client.address.neighborhood);
        if (client.address.street) this.streetFormControl.setValue(client.address.street);
        if (client.address.number) this.numberFormControl.setValue(client.address.number);
      }

      this.idClientUpdate = item.id;
      this.openEdit = true;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar cliente', 'ERRO');
    })
  }

  update() {
    if (this.nameFormControl.valid &&
      this.documentFormControl.valid &&
      this.phoneFormControl.valid &&
      this.emailFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let client: Client = {
        name: this.nameFormControl.value,
        document: this.documentFormControl.value,
        phone: this.phoneFormControl.value,
        email: this.emailFormControl.value,
        address: new Address()
      }

      if (this.countryFormControl.value) client.address.country = this.countryFormControl.value;
      if (this.stateFormControl.value) client.address.state = this.stateFormControl.value;
      if (this.cityFormControl.value) client.address.city = this.cityFormControl.value;
      if (this.cepFormControl.value) client.address.cep = this.cepFormControl.value;
      if (this.neighborhoodFormControl.value) client.address.neighborhood = this.neighborhoodFormControl.value;
      if (this.streetFormControl.value) client.address.street = this.streetFormControl.value;
      if (this.numberFormControl.value) client.address.number = parseInt(this.numberFormControl.value);

      this.clientService.put(this.idClientUpdate, client).then(d => {
        this.idClientUpdate = null;
        this.openEdit = false;
        this.resetForm();
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Cliente alterado com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao alterar cliente', 'ERRO');
      });
    }
  }

  resetForm() {
    this.nameFormControl.reset();
    this.emailFormControl.reset();
    this.documentFormControl.reset();
    this.phoneFormControl.reset();
    this.countryFormControl.reset();
    this.stateFormControl.reset();
    this.cityFormControl.reset();
    this.cepFormControl.reset();
    this.neighborhoodFormControl.reset();
    this.streetFormControl.reset();
    this.numberFormControl.reset();
  }

}
