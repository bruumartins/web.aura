import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from 'src/app/services/util.service';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { LoadingUiServices } from 'src/app/services/loadingUi.services';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../confirm-dialog/confirm-dialog.component'
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Expense } from 'src/app/models/expense';
import { Status } from 'src/app/models/status';
import { ExpenseService } from 'src/app/services/expense.service';
import { ClientService } from 'src/app/services/client.service';
import { StatusService } from 'src/app/services/status.service';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { ProductSale } from 'src/app/models/productSale';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.scss']
})
export class ExpenseComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'title', 'value', 'date', 'description', 'action'];
  dataSource = new MatTableDataSource<Expense>();
  isLogged: boolean = false;
  openPost: boolean = false;
  openEdit: boolean = false;
  status: Array<Status>;
  products: Array<Product>;
  selectedProducts: number[] = null;
  titleFormControl = new FormControl('', [
    Validators.required
  ]);
  descriptionFormControl = new FormControl('', []);
  dateFormControl = new FormControl('', [
    Validators.required
  ]);
  valueFormControl = new FormControl('', [
    Validators.required
  ]);
  statusFormControl = new FormControl('', [
    Validators.required
  ]);
  productFormControl = new FormControl('', []);
  matcher = new MyErrorStateMatcher();
  idExpenseUpdate: number = null;

  constructor(
    private utilService: UtilService,
    public dialog: MatDialog,
    private statusService: StatusService,
    private productService: ProductService,
    private expenseService: ExpenseService
  ) {
    this.isLogged = this.utilService.isLogged();
  }

  ngAfterViewInit() {
    this.get();
    this.getProduct();
    this.getStatus();
  }

  ngOnInit(): void {
  }

  getProduct() {
    this.productService.get().then((results: Array<Product>) => {
      this.products = results;
      console.log('product', results);
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar produto', 'ERRO');
    });
  }

  getStatus() {
    this.statusService.get().then((results: Array<Status>) => {
      this.status = results;
      console.log('status', results);
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar status', 'ERRO');
    })
  }

  get() {
    this.expenseService.get().then((results: Array<Expense>) => {
      this.dataSource = new MatTableDataSource<Expense>(results);
      this.dataSource.paginator = this.paginator;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar despesas', 'ERRO');
    });
  }

  delete(item: any) {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.expenseService.delete(item.id).then(d => {
      this.get();
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Despesa deletada com sucesso', 'SUCESSO');
    }).catch(err => {
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Erro ao deletar despesa', 'ERRO');
    });
  }

  openDialog(item: any) {
    const message = `Realmente deseja excluir a despesa? Essa ação não poderá ser desfeita.`;
    const dialogData = new ConfirmDialogModel("Confirme a exclusão", message);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.delete(item);
      }
    });
  }

  post() {
    if (this.titleFormControl.valid &&
      this.valueFormControl.valid &&
      this.dateFormControl.valid &&
      this.statusFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);

      let expense: Expense = {
        title: this.titleFormControl.value,
        value: this.valueFormControl.value,
        date: this.dateFormControl.value,
        statusId: this.statusFormControl.value
      }
      if (this.descriptionFormControl.value) expense.description = this.descriptionFormControl.value;
      if (this.productFormControl.value) {
        let products: Array<ProductSale> = [];
        this.productFormControl.value.forEach(element => {
          products.push({ productId: element });
        });
        expense.products = products;
      }

      this.expenseService.post(expense).then(d => {
        this.resetForm();
        this.openPost = !this.openPost;
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Despesa inserida com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao inserir despesa', 'ERRO');
      });
    }
  }

  resetForm() {
    this.titleFormControl.reset();
    this.valueFormControl.reset();
    this.dateFormControl.reset();
    this.statusFormControl.reset();
    this.descriptionFormControl.reset();
    this.productFormControl.reset();
  }

  openFormUpdate(item: any) {
    this.expenseService.getById(item.id).then((expense: Expense) => {
      this.titleFormControl.setValue(expense.title);
      this.valueFormControl.setValue(expense.value);
      this.dateFormControl.setValue(expense.date);
      this.statusFormControl.setValue(expense.statusId);

      let auxProduct = [];
      if (expense.products) {
        expense.products.forEach(element => {
          auxProduct.push(element.productId);
        });

        this.selectedProducts = auxProduct;
      }
      if (expense.description) this.descriptionFormControl.setValue(expense.description);

      this.idExpenseUpdate = item.id;
      this.openEdit = true;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar despesa', 'ERRO');
    })
  }

  update() {
    if (this.titleFormControl.valid &&
      this.valueFormControl.valid &&
      this.dateFormControl.valid &&
      this.statusFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);

      let expense: Expense = {
        title: this.titleFormControl.value,
        value: this.valueFormControl.value,
        date: this.dateFormControl.value,
        statusId: this.statusFormControl.value
      }
      if (this.descriptionFormControl.value) expense.description = this.descriptionFormControl.value;
      if (this.productFormControl.value) {
        let products: Array<ProductSale> = [];
        this.productFormControl.value.forEach(element => {
          products.push({ productId: element });
        });
        expense.products = products;
      }

      this.expenseService.put(this.idExpenseUpdate, expense).then(d => {
        this.idExpenseUpdate = null;
        this.openEdit = false;
        this.resetForm();
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Despesa alterada com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao alterar despesa', 'ERRO');
      });
    }
  }
}
