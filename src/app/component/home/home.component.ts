import { Component, OnInit } from '@angular/core';
import { Expense } from 'src/app/models/expense';
import { Product } from 'src/app/models/product';
import { Reminder } from 'src/app/models/reminder';
import { Sale } from 'src/app/models/sale';
import { ReminderService } from 'src/app/services/reminder.service';
import { UtilService } from 'src/app/services/util.service';
import { ExpenseService } from './../../services/expense.service';
import { ProductService } from './../../services/product.service';
import { SaleService } from './../../services/sale.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  isLogged: boolean = false;
  chartView: any[];

  requestVendasConcluido: boolean = false;
  requestDespesaConcluido: boolean = false;

  chartdataVendas: Array<{ name: string, value: any }>;
  chartdataDepesas: Array<{ name: string, value: any }>;
  chartdataProdutos: Array<{ name: string, value: any }>;
  chartdataEventos: Array<{ name: string, value: any }>;
  chartdataLucroMensal: Array<{ name: string, value: any }>;

  months = [
    "Janeiro", "Fevereiro", "Março",
    "Abril", "Maio", "Junho",
    "Julho", "Agosto", "Setembro",
    "Outubro", "Novembro", "Dezembro",
  ];

  constructor(
    private utilService: UtilService,

    private reminderService: ReminderService,
    private saleService: SaleService,
    private productService: ProductService,
    private expenseService: ExpenseService

  ) {
    this.isLogged = this.utilService.isLogged();
    this.chartView = [innerWidth / 1.13, 400]
  }

  ngOnInit(): void {
    this.getDataChartVendas();
    this.getDataChartEventos();
    this.getDataChartDespesas();
    this.getChartProdutos();
  }

  onResize(event) {
    this.chartView = [event.target.innerWidth / 1.13, 400];
  }

  getDataChartVendas() {
    const checkeDateVendas = () => {
      this.requestVendasConcluido = true;
      this.chartdataVendas = [];
      this.checkDataLucroMensal();
    }

    this.saleService.get().then((lista) => {
      if (lista && lista.length) {
        this.dealWithDataSale(lista);
      } else {
        checkeDateVendas();
      }
    }).catch(err => {
      checkeDateVendas();
      this.utilService.openSnackBar('Erro ao buscar dados de vendas', 'ERRO');
    })
  }

  dealWithDataSale(lista: Sale[]) {
    lista = lista.filter(x =>
      new Date(x.date).getFullYear() == new Date().getFullYear()
      && x.statusId == 2
    );
    lista = this.sortArrayByDateAscending(lista, "date");

    let chartDataSale = [];
    lista.forEach(sale => {
      let month = this.months[new Date(sale.date).getMonth()];
      let indexMonthChart = chartDataSale.findIndex(x => x.name == month);
      if (indexMonthChart != -1) {
        chartDataSale[indexMonthChart].value += sale.value;
      } else {
        chartDataSale.push({ name: month, value: sale.value });
      }
    })

    this.chartdataVendas = chartDataSale;
    this.requestVendasConcluido = true;
    this.checkDataLucroMensal();
  }


  getDataChartEventos() {
    this.reminderService.get().then((lista: any[]) => {
      if (lista && lista.length) {
        this.dealWithDataEventos(lista);
      } else {
        this.chartdataEventos = [];
      }
    }).catch(err => {
      this.chartdataEventos = [];
      this.utilService.openSnackBar('Erro ao buscar dados de eventos', 'ERRO');
    })
  }

  dealWithDataEventos(lista: Reminder[]) {
    let chartDataEvento = [];
    lista = lista.filter((evento) => new Date(evento.start).getFullYear() == new Date().getFullYear());
    lista = this.sortArrayByDateAscending(lista, "start");
    lista.forEach(evento => {
      let month = this.months[new Date(evento.start).getMonth()];
      let indexMonthChart = chartDataEvento.findIndex(x => x.name == month);
      if (indexMonthChart != -1) {
        chartDataEvento[indexMonthChart].value += 1;
      } else {
        chartDataEvento.push({ name: month, value: 1 });
      }
    })
    this.chartdataEventos = chartDataEvento;
  }


  getDataChartDespesas() {
    const checkeDateDespesas = () => {
      this.requestDespesaConcluido = true;
      this.chartdataDepesas = [];
      this.checkDataLucroMensal();
    }

    this.expenseService.get().then((lista: any[]) => {
      if (lista && lista.length) {
        this.dealWithDataDespesa(lista);
      } else {
        checkeDateDespesas();
      }
    }).catch(err => {
      checkeDateDespesas();
      this.utilService.openSnackBar('Erro ao buscar dados de despesas', 'ERRO');
    })
  }

  dealWithDataDespesa(lista: Expense[]) {
    let chartDataDepesas = [];
    lista = lista.filter(x => new Date(x.date).getFullYear() == new Date().getFullYear());
    lista = this.sortArrayByDateAscending(lista, "date");

    lista.forEach(despesa => {
      let month = this.months[new Date(despesa.date).getMonth()];
      let indexMonthChart = chartDataDepesas.findIndex(x => x.name == month);
      if (indexMonthChart != -1) {
        chartDataDepesas[indexMonthChart].value += despesa.value;
      } else {
        chartDataDepesas.push({ name: month, value: despesa.value });
      }
    })

    this.chartdataDepesas = chartDataDepesas;
    this.requestDespesaConcluido = true;
    this.checkDataLucroMensal();
  }


  getChartProdutos() {
    this.productService.get().then((lista: any[]) => {
      if (lista.length) {
        this.dealWithDataProduto(lista);
      } else {
        this.chartdataProdutos = [];
      }
    }).catch(err => {
      this.chartdataProdutos = [];
      this.utilService.openSnackBar('Erro ao buscar dados de produtos', 'ERRO');
    });
  }

  dealWithDataProduto(lista: Product[]) {
    let chartDataProduto = [];
    lista.sort((produtoA, produtoB) => {
      return produtoA.quantity > produtoA.quantity ? -1 : 1;
    });
    lista = lista.slice(0, 9);
    lista.sort((produtoA, produtoB) => {
      return produtoA.name > produtoA.name ? -1 : 1;
    });

    lista.forEach(fornecedor => {
      chartDataProduto.push({ name: fornecedor.name, value: (fornecedor.quantity || 0) });
    })
    this.chartdataProdutos = chartDataProduto;
  }


  checkDataLucroMensal() {
    if (this.requestDespesaConcluido && this.requestVendasConcluido) {
      if (!this.chartdataDepesas.length && !this.chartdataVendas.length) {
        this.chartdataLucroMensal = [];
      } else {
        this.dealWithDataLucroMensal();
      }
    }
  }

  dealWithDataLucroMensal() {
    let chartDataLucroMensal = [];
    let dataVendas = this.chartdataVendas;
    let dataDespesas = this.chartdataDepesas;

    const subtract = (num1: number = 0, num2: number = 0) => {
      let result = 0;
      result = num1 > num2 ? num1 - num2 : num2 - num1;
      return result;
    }

    this.months.forEach(month => {
      let valueVendas = dataVendas.find(x => x.name == month);
      let valueDespesas = dataDespesas.find(x => x.name == month);

      if (valueVendas || valueDespesas) {
        let value = subtract((valueDespesas && valueDespesas.value || 0), (valueVendas && valueVendas.value || 0));
        chartDataLucroMensal.push({ name: month, value: value });
      }
    })

    this.chartdataLucroMensal = chartDataLucroMensal;
  }


  private sortArrayByDateAscending(array: Array<any>, nameField: string) {
    array.sort((itemA, itemB) => {
      return new Date(itemB[nameField]) > new Date(itemA[nameField]) ? -1 : 1;
    });
    return array;
  }

}
