import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { SocialUser } from 'angularx-social-login';
import { LoadingUiServices } from 'src/app/services/loadingUi.services';
import { UserService } from 'src/app/services/user.service';
import { SocialService } from '../../services/social.service';
import { Login } from '../../models/login';
import { UserType } from '../../models/userType';
import { UtilService } from 'src/app/services/util.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8)
  ]);
  matcher = new MyErrorStateMatcher();

  constructor(
    private router: Router,
    private userService: UserService,
    private socialService: SocialService,
    private utilService: UtilService
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.emailFormControl.valid && this.passwordFormControl.valid) {
      let login: Login = {
        userEmail: this.emailFormControl.value,
        accessKey: this.passwordFormControl.value,
        grantType: 'password'
      }
      LoadingUiServices.EventoBlockUI.emit(true);
      this.login(login);
    }
  }

  login(login: Login) {
    this.userService.authenticate(login).then((res: any) => {
      localStorage.setItem("token", res.token);
      localStorage.setItem("userType", res.userType);
      let router = "/home";
      if (res.userType == UserType.Admin.toString()) {
        router = "/user";
      }
      this.router.navigate([router], { replaceUrl: true });
    }).catch(error => {
      this.utilService.openSnackBar('Erro ao fazer login', 'ERRO');
    }).finally(() => {
      LoadingUiServices.EventoBlockUI.emit(false);
    })
  }

  loginWithGoogle() {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.socialService.signInGoogle().then((user: SocialUser) => {
      let login: Login = {
        userEmail: user.email,
        accessKey: user.authToken,
        grantType: user.provider.toLocaleLowerCase()
      }
      this.login(login);
    }).catch(error => {
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Erro ao fazer login', 'ERRO');
    })
  }

}
