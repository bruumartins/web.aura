import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  isAdmin: boolean = false;
  constructor(
    private router: Router,
    private socialAuthService: SocialAuthService
  ) {
    this.isAdmin = localStorage.getItem("userType") == "Admin" ? true : false;
  }

  ngOnInit(): void {
  }

  logout() {
    this.socialAuthService.signOut().then(res => {
      console.log(res);
    }).catch(error => {
      console.log(error);
    }).finally(() => {
      localStorage.removeItem('userType');
      localStorage.removeItem('token');
      this.router.navigate(['/'], { replaceUrl: true });
    });
  }
}
