import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Address } from 'src/app/models/address';
import { User } from 'src/app/models/user';
import { LoadingUiServices } from 'src/app/services/loadingUi.services';
import { UserService } from 'src/app/services/user.service';
import { UtilService } from 'src/app/services/util.service';
import { CustomValidator } from '../../utils/customValidators';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../confirm-dialog/confirm-dialog.component';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-my-data',
  templateUrl: './my-data.component.html',
  styleUrls: ['./my-data.component.scss']
})
export class MyDataComponent implements OnInit {
  isLogged: boolean = false;
  formEnable: boolean = false;
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  documentFormControl = new FormControl('', [
    Validators.required,
    CustomValidator.isValidCpf()
  ]);
  countryFormControl = new FormControl('', []);
  stateFormControl = new FormControl('', []);
  cityFormControl = new FormControl('', []);
  cepFormControl = new FormControl('', []);
  neighborhoodFormControl = new FormControl('', []);
  streetFormControl = new FormControl('', []);
  numberFormControl = new FormControl('', []);

  matcher = new MyErrorStateMatcher();

  user: User = null;

  constructor(
    private utilService: UtilService,
    private userService: UserService,
    public dialog: MatDialog,
    private router: Router
  ) {
    this.isLogged = this.utilService.isLogged();

  }

  ngOnInit(): void {
    this.getMyUser();
    this.changeForm("disable");
  }

  getMyUser() {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.userService.getMyUser().then((user: User) => {
      this.user = user;
      this.setUserForm(this.user);
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao consultar usuário', 'ERRO');
    }).finally(() => {
      LoadingUiServices.EventoBlockUI.emit(false);
    })
  }

  setUserForm(user: User) {
    this.emailFormControl.setValue(user.email);
    this.nameFormControl.setValue(user.name);
    this.documentFormControl.setValue(user.document);
    this.countryFormControl.setValue(!user.address ? null : user.address.country);
    this.stateFormControl.setValue(!user.address ? null : user.address.state);
    this.cityFormControl.setValue(!user.address ? null : user.address.city);
    this.cepFormControl.setValue(!user.address ? null : user.address.cep);
    this.neighborhoodFormControl.setValue(!user.address ? null : user.address.neighborhood);
    this.streetFormControl.setValue(!user.address ? null : user.address.street);
    this.numberFormControl.setValue(!user.address ? null : user.address.number);
  }

  changeForm(action: string) {
    if (action == "disable") {
      this.formEnable = false;
      this.emailFormControl.disable();
      this.nameFormControl.disable();
      this.documentFormControl.disable();
      this.countryFormControl.disable();
      this.stateFormControl.disable();
      this.cityFormControl.disable();
      this.cepFormControl.disable();
      this.neighborhoodFormControl.disable();
      this.streetFormControl.disable();
      this.numberFormControl.disable();
    } else {
      this.formEnable = true;
      this.emailFormControl.enable();
      this.nameFormControl.enable();
      this.documentFormControl.enable();
      this.countryFormControl.enable();
      this.stateFormControl.enable();
      this.cityFormControl.enable();
      this.cepFormControl.enable();
      this.neighborhoodFormControl.enable();
      this.streetFormControl.enable();
      this.numberFormControl.enable();
    }
  }

  update() {
    if (this.documentFormControl.valid && this.emailFormControl.valid && this.nameFormControl.valid) {
      if (this.user.address == null) {
        this.user.address = new Address();
      }

      LoadingUiServices.EventoBlockUI.emit(true);
      this.user.document = this.documentFormControl.value;
      this.user.email = this.emailFormControl.value;
      this.user.name = this.nameFormControl.value;
      if (this.countryFormControl.value) this.user.address.country = this.countryFormControl.value;
      if (this.stateFormControl.value) this.user.address.state = this.stateFormControl.value;
      if (this.cityFormControl.value) this.user.address.city = this.cityFormControl.value;
      if (this.cepFormControl.value) this.user.address.cep = this.cepFormControl.value;
      if (this.neighborhoodFormControl.value) this.user.address.neighborhood = this.neighborhoodFormControl.value;
      if (this.streetFormControl.value) this.user.address.street = this.streetFormControl.value;
      if (this.numberFormControl.value) this.user.address.number = this.numberFormControl.value;
      this.userService.put(this.user.id.toString(), this.user).then(d => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Usuário alterado com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao alterar usuário', 'ERRO');
      })
    }
  }

  openDialog(item: any) {
    const message = `Realmente deseja excluir o usuário? Essa ação não poderá ser desfeita.`;
    const dialogData = new ConfirmDialogModel("Confirme a exclusão", message);

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.delete(this.user.id);
      }
    });
  }

  delete(id: number) {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.userService.delete(id).then(d => {
      localStorage.removeItem("token");
      this.router.navigate(['/'], { replaceUrl: true });
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Usuário deletado com sucesso', 'SUCESSO');
    }).catch(err => {
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Erro ao deletar usuário', 'ERRO');
    });
  }

}
