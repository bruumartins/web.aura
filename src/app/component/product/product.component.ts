import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from 'src/app/services/util.service';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { LoadingUiServices } from 'src/app/services/loadingUi.services';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../confirm-dialog/confirm-dialog.component'
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Product } from 'src/app/models/product';
import { ProductService } from '../../services/product.service';
import { ProviderService } from 'src/app/services/provider.service';
import { Provider } from 'src/app/models/provider';
import { ProductProvider } from 'src/app/models/productProvider';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'name', 'saleValue', 'purchaseValue', 'quantity', 'action'];
  dataSource = new MatTableDataSource<Product>();
  isLogged: boolean = false;
  openPost: boolean = false;
  openEdit: boolean = false;
  fornecedores: Array<Provider> = null;
  selectedProviders: number[] = null;
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  codeFormControl = new FormControl('', [
    Validators.required
  ]);
  quantityFormControl = new FormControl('', [
    Validators.required
  ]);
  saleValueFormControl = new FormControl('', [
    Validators.required
  ]);
  purchaseValueFormControl = new FormControl('', [
    Validators.required
  ]);
  providerFormControl = new FormControl('', [
    Validators.required
  ]);
  matcher = new MyErrorStateMatcher();
  idProductUpdate: number = null;

  constructor(
    private productService: ProductService,
    private utilService: UtilService,
    public dialog: MatDialog,
    private providerService: ProviderService,
  ) {
    this.isLogged = this.utilService.isLogged();
  }

  ngAfterViewInit() {
    this.get();
    this.getProvider();
  }

  ngOnInit(): void {
  }

  getProvider() {
    this.providerService.get().then((results: Array<Provider>) => {
      this.fornecedores = results;
      this.dataSource.paginator = this.paginator;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar fornecedor', 'ERRO');
    });
  }

  get() {
    this.productService.get().then((results: Array<Product>) => {
      this.dataSource = new MatTableDataSource<Product>(results);
      this.dataSource.paginator = this.paginator;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar produto', 'ERRO');
    });
  }

  delete(item: any) {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.productService.delete(item.id).then(d => {
      this.get();
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Produto deletado com sucesso', 'SUCESSO');
    }).catch(err => {
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Erro ao deletar produto', 'ERRO');
    });
  }

  openDialog(item: any) {
    const message = `Realmente deseja excluir o produto? Essa ação não poderá ser desfeita.`;
    const dialogData = new ConfirmDialogModel("Confirme a exclusão", message);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.delete(item);
      }
    });
  }

  post() {
    if (this.nameFormControl.valid &&
      this.codeFormControl.valid &&
      this.quantityFormControl.valid &&
      this.saleValueFormControl.valid &&
      this.purchaseValueFormControl.valid &&
      this.providerFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let providers: Array<ProductProvider> = [];
      this.providerFormControl.value.forEach(element => {
        providers.push({ providerId: element });
      });

      let product: Product = {
        name: this.nameFormControl.value,
        code: this.codeFormControl.value,
        quantity: this.quantityFormControl.value,
        purchaseValue: this.purchaseValueFormControl.value,
        saleValue: this.saleValueFormControl.value,
        providers: providers
      }

      this.productService.post(product).then(d => {
        this.resetForm();
        this.openPost = !this.openPost;
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Produto inserido com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao inserir produto', 'ERRO');
      });
    }
  }

  resetForm() {
    this.nameFormControl.reset(),
      this.codeFormControl.reset(),
      this.quantityFormControl.reset(),
      this.purchaseValueFormControl.reset(),
      this.saleValueFormControl.reset(),
      this.providerFormControl.reset()
  }

  openFormUpdate(item: any) {
    this.productService.getById(item.id).then((product: Product) => {
      this.nameFormControl.setValue(product.name);
      this.codeFormControl.setValue(product.code);
      this.quantityFormControl.setValue(product.quantity);
      this.purchaseValueFormControl.setValue(product.purchaseValue);
      this.saleValueFormControl.setValue(product.saleValue);
      let auxProvider = [];
      if (product.providers) {
        product.providers.forEach(element => {
          auxProvider.push(element.providerId);
        });
        this.selectedProviders = auxProvider;
      }

      this.idProductUpdate = item.id;
      this.openEdit = true;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar produto', 'ERRO');
    })
  }

  update() {
    if (this.nameFormControl.valid &&
      this.codeFormControl.valid &&
      this.quantityFormControl.valid &&
      this.saleValueFormControl.valid &&
      this.purchaseValueFormControl.valid &&
      this.providerFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let providers: Array<ProductProvider> = [];
      this.providerFormControl.value.forEach(element => {
        providers.push({ providerId: element });
      });
      let product: Product = {
        name: this.nameFormControl.value,
        code: this.codeFormControl.value,
        quantity: this.quantityFormControl.value,
        purchaseValue: this.purchaseValueFormControl.value,
        saleValue: this.saleValueFormControl.value,
        providers: providers
      }

      this.productService.put(this.idProductUpdate, product).then(d => {
        this.idProductUpdate = null;
        this.openEdit = false;
        this.resetForm();
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Produto alterado com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao alterar produto', 'ERRO');
      });
    }
  }


}
