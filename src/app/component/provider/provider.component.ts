import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from 'src/app/services/util.service';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { LoadingUiServices } from 'src/app/services/loadingUi.services';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../confirm-dialog/confirm-dialog.component'
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { ProviderService } from '../../services/provider.service';
import { Provider } from '../../models/provider';
import { CustomValidator } from '../../utils/customValidators';
import { Address } from 'src/app/models/address';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  isLogged: boolean = false;
  displayedColumns: string[] = ['id', 'name', 'document', 'phone', 'action'];
  dataSource = new MatTableDataSource<Provider>();
  openPost: boolean = false;
  openEdit: boolean = false;
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  phoneFormControl = new FormControl('', []);
  documentFormControl = new FormControl('', [
    CustomValidator.isValidCpf()
  ]);
  countryFormControl = new FormControl('', []);
  stateFormControl = new FormControl('', []);
  cityFormControl = new FormControl('', []);
  cepFormControl = new FormControl('', []);
  neighborhoodFormControl = new FormControl('', []);
  streetFormControl = new FormControl('', []);
  numberFormControl = new FormControl('', []);
  matcher = new MyErrorStateMatcher();
  idProviderUpdate: number = null;

  constructor(
    private providerService: ProviderService,
    private utilService: UtilService,
    public dialog: MatDialog
  ) {
    this.isLogged = this.utilService.isLogged();
  }

  ngAfterViewInit() {
    this.get();
  }

  ngOnInit(): void {
  }

  get() {
    this.providerService.get().then((results: Array<Provider>) => {
      this.dataSource = new MatTableDataSource<Provider>(results);
      this.dataSource.paginator = this.paginator;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar fornecedor', 'ERRO');
    });
  }

  post() {
    if (this.nameFormControl.valid &&
      this.documentFormControl.valid &&
      this.phoneFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let provider: Provider = {
        name: this.nameFormControl.value,
        document: this.documentFormControl.value,
        phone: this.phoneFormControl.value,
        address: new Address()
      }

      if (this.countryFormControl.value) provider.address.country = this.countryFormControl.value;
      if (this.stateFormControl.value) provider.address.state = this.stateFormControl.value;
      if (this.cityFormControl.value) provider.address.city = this.cityFormControl.value;
      if (this.cepFormControl.value) provider.address.cep = this.cepFormControl.value;
      if (this.neighborhoodFormControl.value) provider.address.neighborhood = this.neighborhoodFormControl.value;
      if (this.streetFormControl.value) provider.address.street = this.streetFormControl.value;
      if (this.numberFormControl.value) provider.address.number = this.numberFormControl.value;

      this.providerService.post(provider).then(d => {
        this.resetForm();
        this.openPost = !this.openPost;
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Fornecedor inserido com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao inserir fornecedor', 'ERRO');
      });
    }
  }

  delete(item: any) {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.providerService.delete(item.id).then(d => {
      this.get();
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Fornecedor deletado com sucesso', 'SUCESSO');
    }).catch(err => {
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Erro ao deletar fornecedor', 'ERRO');
    });
  }

  resetForm() {
    this.nameFormControl.reset();
    this.documentFormControl.reset();
    this.phoneFormControl.reset();
    this.countryFormControl.reset();
    this.stateFormControl.reset();
    this.cityFormControl.reset();
    this.cepFormControl.reset();
    this.neighborhoodFormControl.reset();
    this.streetFormControl.reset();
    this.numberFormControl.reset();
  }

  openDialog(item: any) {
    const message = `Realmente deseja excluir o fornecedor? Essa ação não poderá ser desfeita.`;
    const dialogData = new ConfirmDialogModel("Confirme a exclusão", message);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.delete(item);
      }
    });
  }

  openFormUpdate(item: any) {
    this.providerService.getById(item.id).then((provider: Provider) => {
      this.nameFormControl.setValue(provider.name);
      this.documentFormControl.setValue(provider.document);
      this.phoneFormControl.setValue(provider.phone);
      if (provider.address) {
        if (provider.address.country) this.countryFormControl.setValue(provider.address.country);
        if (provider.address.state) this.stateFormControl.setValue(provider.address.state);
        if (provider.address.city) this.cityFormControl.setValue(provider.address.city);
        if (provider.address.cep) this.cepFormControl.setValue(provider.address.cep);
        if (provider.address.neighborhood) this.neighborhoodFormControl.setValue(provider.address.neighborhood);
        if (provider.address.street) this.streetFormControl.setValue(provider.address.street);
        if (provider.address.number) this.numberFormControl.setValue(provider.address.number);
      }

      this.idProviderUpdate = item.id;
      this.openEdit = true;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar fornecedor', 'ERRO');
    })
  }

  update() {
    if (this.nameFormControl.valid &&
      this.documentFormControl.valid &&
      this.phoneFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let provider: Provider = {
        name: this.nameFormControl.value,
        document: this.documentFormControl.value,
        phone: this.phoneFormControl.value,
        address: new Address()
      }

      if (this.countryFormControl.value) provider.address.country = this.countryFormControl.value;
      if (this.stateFormControl.value) provider.address.state = this.stateFormControl.value;
      if (this.cityFormControl.value) provider.address.city = this.cityFormControl.value;
      if (this.cepFormControl.value) provider.address.cep = this.cepFormControl.value;
      if (this.neighborhoodFormControl.value) provider.address.neighborhood = this.neighborhoodFormControl.value;
      if (this.streetFormControl.value) provider.address.street = this.streetFormControl.value;
      if (this.numberFormControl.value) provider.address.number = parseInt(this.numberFormControl.value);

      this.providerService.put(this.idProviderUpdate, provider).then(d => {
        this.idProviderUpdate = null;
        this.openEdit = false;
        this.resetForm();
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Fornecedor alterado com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao alterar fornecedor', 'ERRO');
      });
    }
  }
}
