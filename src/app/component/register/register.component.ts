import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { SocialUser } from 'angularx-social-login';
import { Address } from 'src/app/models/address';
import { SocialService } from 'src/app/services/social.service';
import { UtilService } from 'src/app/services/util.service';
import { UserType } from '../../models/userType';
import { CustomValidator } from '../../utils/customValidators';
import { User } from './../../models/user';
import { LoadingUiServices } from './../../services/loadingUi.services';
import { UserService } from './../../services/user.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  hide = true;
  hideConfirm = true;
  user = new User();
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8)
  ]);
  confirmPasswordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8),
    CustomValidator.passwordMatch(this.passwordFormControl)
  ]);
  documentFormControl = new FormControl('', [
    Validators.required,
    CustomValidator.isValidCpf()
  ]);

  countryFormControl = new FormControl('', []);
  stateFormControl = new FormControl('', []);
  cityFormControl = new FormControl('', []);
  cepFormControl = new FormControl('', []);
  neighborhoodFormControl = new FormControl('', []);
  streetFormControl = new FormControl('', []);
  numberFormControl = new FormControl('', []);

  matcher = new MyErrorStateMatcher();

  constructor(
    private userService: UserService,
    private router: Router,
    private socialService: SocialService,
    private utilService: UtilService
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (
      this.nameFormControl.valid &&
      this.emailFormControl.valid &&
      this.passwordFormControl.valid &&
      this.confirmPasswordFormControl.valid &&
      this.documentFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      this.registerUser();
    }
  }

  registerUser() {
    this.user.address = new Address();
    this.user.name = this.nameFormControl.value;
    this.user.email = this.emailFormControl.value;
    this.user.password = this.passwordFormControl.value;
    this.user.name = this.nameFormControl.value;
    this.user.document = this.documentFormControl.value;

    if (this.countryFormControl.value) this.user.address.country = this.countryFormControl.value;
    if (this.stateFormControl.value) this.user.address.state = this.stateFormControl.value;
    if (this.cityFormControl.value) this.user.address.city = this.cityFormControl.value;
    if (this.cepFormControl.value) this.user.address.cep = this.cepFormControl.value;
    if (this.neighborhoodFormControl.value) this.user.address.neighborhood = this.neighborhoodFormControl.value;
    if (this.streetFormControl.value) this.user.address.street = this.streetFormControl.value;
    if (this.numberFormControl.value) this.user.address.number = this.numberFormControl.value;

    this.userService.post(this.user).then((result: any) => {
      localStorage.setItem("token", result.token);
      let router = "/home";
      if (result.userType == UserType.Admin.toString()) {
        router = "/user";
      }
      this.router.navigate([router], { replaceUrl: true });
    }).catch(error => {
      this.utilService.openSnackBar('Erro ao registrar usuário', 'ERRO');
    }).finally(() => {
      LoadingUiServices.EventoBlockUI.emit(false);
    })
  }

  registerUserWithGoogle() {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.socialService.signInGoogle().then((user: SocialUser) => {
      this.user.googleGuid = user.id;
      this.nameFormControl.setValue(user.name);
      this.emailFormControl.setValue(user.email);
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao registrar usuário', 'ERRO');
    }).finally(() => {
      LoadingUiServices.EventoBlockUI.emit(false);
    });
  }

}
