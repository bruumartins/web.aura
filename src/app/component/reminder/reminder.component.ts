import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from 'src/app/services/util.service';
import { Reminder } from '../../models/reminder';
import { ReminderService } from '../../services/reminder.service';
import { LoadingUiServices } from 'src/app/services/loadingUi.services';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../confirm-dialog/confirm-dialog.component';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.component.html',
  styleUrls: ['./reminder.component.scss']
})
export class ReminderComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'title', 'start', 'end', 'description', 'action'];
  dataSource = new MatTableDataSource<Reminder>();
  isLogged: boolean = false;
  openPost: boolean = false;
  openEdit: boolean = false;
  titleFormControl = new FormControl('', [
    Validators.required
  ]);
  startFormControl = new FormControl('', [
    Validators.required
  ]);
  endFormControl = new FormControl('', [
    Validators.required
  ]);
  descriptionFormControl = new FormControl('', [
    Validators.required
  ]);
  matcher = new MyErrorStateMatcher();
  idReminderUpdate: number = null;

  constructor(
    private reminderService: ReminderService,
    private utilService: UtilService,
    public dialog: MatDialog
  ) {
    this.isLogged = this.utilService.isLogged();
  }

  ngAfterViewInit() {
    this.get();
  }

  ngOnInit(): void { }

  get() {
    this.reminderService.get().then((results: Array<Reminder>) => {
      this.dataSource = new MatTableDataSource<Reminder>(results);
      this.dataSource.paginator = this.paginator;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar agenda', 'ERRO');
    });
  }

  post() {
    if (this.titleFormControl.valid &&
      this.startFormControl.valid &&
      this.endFormControl.valid &&
      this.descriptionFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let reminder: Reminder = {
        title: this.titleFormControl.value,
        start: this.startFormControl.value,
        end: this.endFormControl.value,
        description: this.descriptionFormControl.value
      }

      this.reminderService.post(reminder).then(d => {
        this.resetForm();
        this.openPost = !this.openPost;
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Evento inserido com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao inserir evento', 'ERRO');
      });
    }
  }

  resetForm() {
    this.titleFormControl.reset();
    this.startFormControl.reset();
    this.endFormControl.reset();
    this.descriptionFormControl.reset();
  }

  delete(item: any) {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.reminderService.delete(item.id).then(d => {
      this.get();
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Evento deletado com sucesso', 'SUCESSO');
    }).catch(err => {
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Erro ao deletar evento', 'ERRO');
    });
  }

  openDialog(item: any) {
    const message = `Realmente deseja excluir o evento? Essa ação não poderá ser desfeita.`;
    const dialogData = new ConfirmDialogModel("Confirme a exclusão", message);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.delete(item);
      }
    });
  }

  openFormUpdate(item: any) {
    this.reminderService.getById(item.id).then((reminder: Reminder) => {
      this.titleFormControl.setValue(reminder.title);
      this.startFormControl.setValue(reminder.start);
      this.endFormControl.setValue(reminder.end);
      this.descriptionFormControl.setValue(reminder.description);

      this.idReminderUpdate = item.id;
      this.openEdit = true;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar evento', 'ERRO');
    })
  }

  update() {
    if (this.titleFormControl.valid &&
      this.startFormControl.valid &&
      this.endFormControl.valid &&
      this.descriptionFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let reminder: Reminder = {
        title: this.titleFormControl.value,
        start: this.startFormControl.value,
        end: this.endFormControl.value,
        description: this.descriptionFormControl.value
      }

      this.reminderService.put(this.idReminderUpdate, reminder).then(d => {
        this.idReminderUpdate = null;
        this.openEdit = false;
        this.resetForm();
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Evento alterado com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao alterar evento', 'ERRO');
      });
    }
  }

}
