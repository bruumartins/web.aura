import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from 'src/app/services/util.service';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { LoadingUiServices } from 'src/app/services/loadingUi.services';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../confirm-dialog/confirm-dialog.component'
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Sale } from 'src/app/models/sale';
import { Client } from 'src/app/models/client';
import { Status } from 'src/app/models/status';
import { SaleService } from 'src/app/services/sale.service';
import { ClientService } from 'src/app/services/client.service';
import { StatusService } from 'src/app/services/status.service';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { ProductSale } from 'src/app/models/productSale';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.scss']
})
export class SaleComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'name', 'value', 'date', 'description', 'action'];
  dataSource = new MatTableDataSource<Sale>();
  isLogged: boolean = false;
  openPost: boolean = false;
  openEdit: boolean = false;
  clients: Array<Client>;
  status: Array<Status>;
  products: Array<Product>;
  selectedProducts: number[] = null;
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  descriptionFormControl = new FormControl('', []);
  dateFormControl = new FormControl('', [
    Validators.required
  ]);
  valueFormControl = new FormControl('', [
    Validators.required
  ]);
  clientFormControl = new FormControl('', []);
  statusFormControl = new FormControl('', [
    Validators.required
  ]);
  productFormControl = new FormControl('', [
    Validators.required
  ]);
  matcher = new MyErrorStateMatcher();
  idSaleUpdate: number = null;

  constructor(
    private utilService: UtilService,
    public dialog: MatDialog,
    private saleService: SaleService,
    private clientService: ClientService,
    private statusService: StatusService,
    private productService: ProductService
  ) {
    this.isLogged = this.utilService.isLogged();
  }

  ngAfterViewInit() {
    this.get();
    this.getClient();
    this.getProduct();
    this.getStatus();
  }

  ngOnInit(): void {
  }

  getProduct() {
    this.productService.get().then((results: Array<Product>) => {
      this.products = results;
      console.log('product', results);
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar produto', 'ERRO');
    });
  }

  getClient() {
    this.clientService.get().then((results: Array<Client>) => {
      this.clients = results;
      console.log('clients', results);
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar cliente', 'ERRO');
    });
  }

  getStatus() {
    this.statusService.get().then((results: Array<Status>) => {
      this.status = results;
      console.log('status', results);
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar status', 'ERRO');
    })
  }

  get() {
    this.saleService.get().then((results: Array<Sale>) => {
      this.dataSource = new MatTableDataSource<Sale>(results);
      this.dataSource.paginator = this.paginator;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar vendas', 'ERRO');
    });
  }

  delete(item: any) {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.saleService.delete(item.id).then(d => {
      this.get();
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Venda deletada com sucesso', 'SUCESSO');
    }).catch(err => {
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Erro ao deletar venda', 'ERRO');
    });
  }

  openDialog(item: any) {
    const message = `Realmente deseja excluir a venda? Essa ação não poderá ser desfeita.`;
    const dialogData = new ConfirmDialogModel("Confirme a exclusão", message);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.delete(item);
      }
    });
  }

  post() {
    if (this.nameFormControl.valid &&
      this.valueFormControl.valid &&
      this.dateFormControl.valid &&
      this.statusFormControl.valid &&
      this.productFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let products: Array<ProductSale> = [];
      this.productFormControl.value.forEach(element => {
        products.push({ productId: element });
      });

      let sale: Sale = {
        name: this.nameFormControl.value,
        value: this.valueFormControl.value,
        date: this.dateFormControl.value,
        statusId: this.statusFormControl.value,
        products: products
      }
      if (this.clientFormControl.value) sale.clientId = this.clientFormControl.value;
      if (this.descriptionFormControl.value) sale.description = this.descriptionFormControl.value;

      this.saleService.post(sale).then(d => {
        this.resetForm();
        this.openPost = !this.openPost;
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Venda inserida com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao inserir venda', 'ERRO');
      });
    }
  }

  resetForm() {
    this.nameFormControl.reset();
    this.valueFormControl.reset();
    this.dateFormControl.reset();
    this.statusFormControl.reset();
    this.descriptionFormControl.reset();
    this.productFormControl.reset();
    this.clientFormControl.reset();
  }

  openFormUpdate(item: any) {
    this.saleService.getById(item.id).then((sale: Sale) => {
      this.nameFormControl.setValue(sale.name);
      this.valueFormControl.setValue(sale.value);
      this.dateFormControl.setValue(sale.date);
      this.statusFormControl.setValue(sale.statusId);

      let auxProduct = [];
      if (sale.products) {
        sale.products.forEach(element => {
          auxProduct.push(element.productId);
        });

        this.selectedProducts = auxProduct;
      }
      if (sale.clientId) this.clientFormControl.setValue(sale.clientId);
      if (sale.description) this.descriptionFormControl.setValue(sale.description);

      this.idSaleUpdate = item.id;
      this.openEdit = true;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar venda', 'ERRO');
    })
  }

  update() {
    if (this.nameFormControl.valid &&
      this.valueFormControl.valid &&
      this.dateFormControl.valid &&
      this.statusFormControl.valid &&
      this.productFormControl.valid
    ) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let products: Array<ProductSale> = [];
      this.productFormControl.value.forEach(element => {
        products.push({ productId: element });
      });

      let sale: Sale = {
        name: this.nameFormControl.value,
        value: this.valueFormControl.value,
        date: this.dateFormControl.value,
        statusId: this.statusFormControl.value,
        products: products
      }
      if (this.clientFormControl.value) sale.clientId = this.clientFormControl.value;
      if (this.descriptionFormControl.value) sale.description = this.descriptionFormControl.value;

      this.saleService.put(this.idSaleUpdate, sale).then(d => {
        this.idSaleUpdate = null;
        this.openEdit = false;
        this.resetForm();
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Venda alterada com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao alterar venda', 'ERRO');
      });
    }
  }

}
