import { Component, OnInit, ViewChild } from '@angular/core';
import { StatusService } from '../../services/status.service'
import { Status } from '../../models/status';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from 'src/app/services/util.service';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { LoadingUiServices } from 'src/app/services/loadingUi.services';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../confirm-dialog/confirm-dialog.component'
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'title', 'action'];
  dataSource = new MatTableDataSource<Status>();
  isLogged: boolean = false;
  openPost: boolean = false;
  openEdit: boolean = false;
  titleFormControl = new FormControl('', [
    Validators.required
  ]);
  matcher = new MyErrorStateMatcher();
  idStatusUpdate: number = null;

  constructor(
    private statusService: StatusService,
    private utilService: UtilService,
    public dialog: MatDialog
  ) {
    this.isLogged = this.utilService.isLogged();
  }

  ngAfterViewInit() {
    this.get();
  }

  ngOnInit(): void { }

  post() {
    if (this.titleFormControl.valid) {
      LoadingUiServices.EventoBlockUI.emit(true);
      let status: Status = {
        title: this.titleFormControl.value
      }

      this.statusService.post(status).then(d => {
        this.titleFormControl.reset();
        this.openPost = !this.openPost;
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Status inserido com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao inserir status', 'ERRO');
      });
    }
  }

  get() {
    this.statusService.get().then((results: Array<Status>) => {
      this.dataSource = new MatTableDataSource<Status>(results);
      this.dataSource.paginator = this.paginator;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar status', 'ERRO');
    })
  }


  openFormUpdate(item: any) {
    this.titleFormControl.setValue(item.title);
    this.idStatusUpdate = item.id;
    this.openEdit = true;
  }

  update() {
    if (this.titleFormControl.valid) {
      let status: Status = {
        title: this.titleFormControl.value
      }

      this.statusService.put(this.idStatusUpdate, status).then(d => {
        this.idStatusUpdate = null;
        this.openEdit = false;
        this.titleFormControl.reset();
        this.get();
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Status alterado com sucesso', 'SUCESSO');
      }).catch(err => {
        LoadingUiServices.EventoBlockUI.emit(false);
        this.utilService.openSnackBar('Erro ao alterar status', 'ERRO');
      });
    }
  }

  delete(item: any) {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.statusService.delete(item.id).then(d => {
      this.get();
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Status deletado com sucesso', 'SUCESSO');
    }).catch(err => {
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Erro ao deletar status', 'ERRO');
    });
  }

  openDialog(item: any) {
    const message = `Realmente deseja excluir o status? Essa ação não poderá ser desfeita.`;
    const dialogData = new ConfirmDialogModel("Confirme a exclusão", message);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.delete(item);
      }
    });
  }
}
