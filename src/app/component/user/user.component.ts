import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from 'src/app/services/util.service';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { LoadingUiServices } from 'src/app/services/loadingUi.services';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../confirm-dialog/confirm-dialog.component'
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'name', 'email', 'document', 'type', 'action'];
  dataSource = new MatTableDataSource<User>();
  isLogged: boolean = false;
  matcher = new MyErrorStateMatcher();

  constructor(
    private utilService: UtilService,
    private userService: UserService,
    public dialog: MatDialog
  ) {
    this.isLogged = this.utilService.isLogged();
  }

  ngAfterViewInit() {
    this.get();
  }

  ngOnInit(): void { }

  get() {
    this.userService.get().then((results: Array<User>) => {
      this.dataSource = new MatTableDataSource<User>(results);
      this.dataSource.paginator = this.paginator;
    }).catch(err => {
      this.utilService.openSnackBar('Erro ao buscar usuários', 'ERRO');
    });
  }

  delete(item: any) {
    LoadingUiServices.EventoBlockUI.emit(true);
    this.userService.delete(item.id).then(d => {
      this.get();
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Usuário deletado com sucesso', 'SUCESSO');
    }).catch(err => {
      LoadingUiServices.EventoBlockUI.emit(false);
      this.utilService.openSnackBar('Erro ao deletar usuário', 'ERRO');
    });
  }

  openDialog(item: any) {
    const message = `Realmente deseja excluir o usuário? Essa ação não poderá ser desfeita.`;
    const dialogData = new ConfirmDialogModel("Confirme a exclusão", message);

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.delete(item);
      }
    });
  }

}
