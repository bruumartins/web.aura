export class Address {
    cep?: string;
    city?: string;
    clientId?: number;
    country?: string;
    created?: string;
    id?: number;
    modified?: string;
    neighborhood?: string;
    number?: number;
    providerId?: number;
    state?: string;
    street?: string;
    userId?: number;
}