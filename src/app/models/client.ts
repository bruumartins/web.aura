import { Address } from '../models/address';
export class Client {
  id?: number;
  name: string;
  document?: string;
  phone?: string;
  email?: string;
  address?: Address;
}