import { User } from '../models/user';
import { ProductSale } from './productSale';
import { Status } from './status';

export class Expense {
    id?: number;
    title: string;
    value: number;
    date: Date;
    description?: string;
    userId?: number;
    user?: User;
    statusId?: number;
    status?: Status;
    products?: Array<ProductSale>;
}