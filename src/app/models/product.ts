import { ProductProvider } from '../models/productProvider';
export class Product {
  id?: number;
  name: string;
  code: string;
  saleValue: number;
  purchaseValue: number;
  quantity: number;
  providers?: Array<ProductProvider>;
}