import { Product } from 'src/app/models/product';
import { Address } from '../models/address';
export class Provider {
  id?: number;
  name: string;
  document?: string;
  phone?: string;
  address?: Address;
  products?: Product[];
}