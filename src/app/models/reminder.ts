export class Reminder {
  id?: number;
  title: string;
  start: Date;
  end: Date;
  description: string;
  userId?: number;
}