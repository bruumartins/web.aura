import { User } from '../models/user';
import { Client } from "./client";
import { ProductSale } from './productSale';
import { Status } from './status';

export class Sale {
    id?: number;
    name: string;
    value: number;
    date: Date;
    description?: string;
    clientId?: number;
    client?: Client
    userId?: number;
    user?: User;
    statusId?: number;
    status?: Status;
    products?: Array<ProductSale>;
}