import { UserType } from '../models/userType';
import { Address } from '../models/address';
export class User {
    id?: number;
    name: string;
    email: string;
    document: string;
    password: string;
    googleGuid: string;
    type: UserType;
    address: Address;
}