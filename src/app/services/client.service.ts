import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as Config } from '../../environments/environment';
import { Client } from '../models/client';
import { UtilService } from '../services/util.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(
    private httpClient: HttpClient,
    private utilService: UtilService
  ) { }

  get() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/client/GetAllByUserId`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  post(client: Client) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${Config.urlAPI}/client`, client, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  put(id: number, client: Client) {
    return new Promise((resolve, reject) => {
      this.httpClient.put(`${Config.urlAPI}/client/${id}`, client, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  delete(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(`${Config.urlAPI}/client/${id}`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  getById(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/client/${id}`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

}
