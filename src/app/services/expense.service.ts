import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as Config } from '../../environments/environment';
import { Expense } from '../models/expense';
import { UtilService } from './util.service';

@Injectable({
    providedIn: 'root'
})

export class ExpenseService {

    constructor(
        private httpClient: HttpClient,
        private utilService: UtilService
    ) { }

    get(): Promise<Expense[]> {
        return new Promise((resolve, reject) => {
            this.httpClient.get(`${Config.urlAPI}/expense/GetAllByUserId`, this.utilService.headerPadrao()).subscribe((d: any) => {
                resolve(d);
            }, error => {
                this.utilService.dealWithRequestCatch(error, reject);
            });
        });
    }

    post(expense: Expense) {
        return new Promise((resolve, reject) => {
            this.httpClient.post(`${Config.urlAPI}/expense`, expense, this.utilService.headerPadrao()).subscribe(d => {
                resolve(d);
            }, error => {
                this.utilService.dealWithRequestCatch(error, reject);
            });
        });
    }

    put(id: number, expense: Expense) {
        return new Promise((resolve, reject) => {
            this.httpClient.put(`${Config.urlAPI}/expense/${id}`, expense, this.utilService.headerPadrao()).subscribe(d => {
                resolve(d);
            }, error => {
                this.utilService.dealWithRequestCatch(error, reject);
            });
        });
    }

    delete(id: number) {
        return new Promise((resolve, reject) => {
            this.httpClient.delete(`${Config.urlAPI}/expense/${id}`, this.utilService.headerPadrao()).subscribe(d => {
                resolve(d);
            }, error => {
                this.utilService.dealWithRequestCatch(error, reject);
            });
        });
    }

    getById(id: number) {
        return new Promise((resolve, reject) => {
            this.httpClient.get(`${Config.urlAPI}/expense/${id}`, this.utilService.headerPadrao()).subscribe(d => {
                resolve(d);
            }, error => {
                this.utilService.dealWithRequestCatch(error, reject);
            });
        });
    }

}
