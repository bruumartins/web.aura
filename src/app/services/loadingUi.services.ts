import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class LoadingUiServices {

    static EventoBlockUI = new EventEmitter<boolean>();
    constructor() { }
}