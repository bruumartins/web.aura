import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as Config } from '../../environments/environment';
import { UtilService } from '../services/util.service';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private httpClient: HttpClient,
    private utilService: UtilService
  ) { }

  get() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/product/GetAllByUserId`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, err => {
        this.utilService.dealWithRequestCatch(err, reject);
      });
    });
  }

  post(product: Product) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${Config.urlAPI}/product`, product, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, err => {
        this.utilService.dealWithRequestCatch(err, reject);
      });
    });
  }

  put(id: number, product: Product) {
    return new Promise((resolve, reject) => {
      this.httpClient.put(`${Config.urlAPI}/product/${id}`, product, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, err => {
        this.utilService.dealWithRequestCatch(err, reject);
      });
    });
  }

  delete(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(`${Config.urlAPI}/product/${id}`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, err => {
        this.utilService.dealWithRequestCatch(err, reject);
      });
    });
  }

  getById(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/product/${id}`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, err => {
        this.utilService.dealWithRequestCatch(err, reject);
      });
    });
  }

}
