import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as Config } from '../../environments/environment';
import { Provider } from '../models/provider';
import { UtilService } from '../services/util.service';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  constructor(
    private httpClient: HttpClient,
    private utilService: UtilService
  ) { }

  get() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/provider/GetAllByUserId`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  post(provider: Provider) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${Config.urlAPI}/provider`, provider, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  put(id: number, provider: Provider) {
    return new Promise((resolve, reject) => {
      this.httpClient.put(`${Config.urlAPI}/provider/${id}`, provider, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  delete(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(`${Config.urlAPI}/provider/${id}`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  getById(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/provider/${id}`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }
}
