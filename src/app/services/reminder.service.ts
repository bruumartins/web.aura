import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as Config } from '../../environments/environment';
import { Reminder } from '../models/reminder';
import { UtilService } from '../services/util.service';

@Injectable({
  providedIn: 'root'
})
export class ReminderService {

  constructor(
    private httpClient: HttpClient,
    private utilService: UtilService
  ) { }

  get() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/reminder/GetAllByUserId`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  post(reminder: Reminder) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${Config.urlAPI}/reminder`, reminder, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  put(id: number, reminder: Reminder) {
    return new Promise((resolve, reject) => {
      this.httpClient.put(`${Config.urlAPI}/reminder/${id}`, reminder, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  delete(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(`${Config.urlAPI}/reminder/${id}`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  getById(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/reminder/${id}`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }
}
