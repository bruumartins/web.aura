import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as Config } from '../../environments/environment';
import { Sale } from '../models/sale';
import { UtilService } from './util.service';

@Injectable({
    providedIn: 'root'
})

export class SaleService {

    constructor(
        private httpClient: HttpClient,
        private utilService: UtilService
    ) { }

    get(): Promise<Sale[]> {
        return new Promise((resolve, reject) => {
            this.httpClient.get(`${Config.urlAPI}/sale/GetAllByUserId`, this.utilService.headerPadrao()).subscribe((d: any) => {
                resolve(d);
            }, error => {
                this.utilService.dealWithRequestCatch(error, reject);
            });
        });
    }

    post(sale: Sale) {
        return new Promise((resolve, reject) => {
            this.httpClient.post(`${Config.urlAPI}/sale`, sale, this.utilService.headerPadrao()).subscribe(d => {
                resolve(d);
            }, error => {
                this.utilService.dealWithRequestCatch(error, reject);
            });
        });
    }

    put(id: number, sale: Sale) {
        return new Promise((resolve, reject) => {
            this.httpClient.put(`${Config.urlAPI}/sale/${id}`, sale, this.utilService.headerPadrao()).subscribe(d => {
                resolve(d);
            }, error => {
                this.utilService.dealWithRequestCatch(error, reject);
            });
        });
    }

    delete(id: number) {
        return new Promise((resolve, reject) => {
            this.httpClient.delete(`${Config.urlAPI}/sale/${id}`, this.utilService.headerPadrao()).subscribe(d => {
                resolve(d);
            }, error => {
                this.utilService.dealWithRequestCatch(error, reject);
            });
        });
    }

    getById(id: number) {
        return new Promise((resolve, reject) => {
            this.httpClient.get(`${Config.urlAPI}/sale/${id}`, this.utilService.headerPadrao()).subscribe(d => {
                resolve(d);
            }, error => {
                this.utilService.dealWithRequestCatch(error, reject);
            });
        });
    }

}
