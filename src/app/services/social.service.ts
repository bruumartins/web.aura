import { Injectable } from '@angular/core';
import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { UtilService } from './util.service';

@Injectable({
    providedIn: 'root'
})
export class SocialService {

    constructor(
        private socialAuthService: SocialAuthService,
        private utilService: UtilService
    ) {

    }

    signInGoogle() {
        return new Promise((resolve, reject) => {
            this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
            this.socialAuthService.authState.subscribe((user) => {
                if (user)
                    resolve(user);
                else {
                    reject(user);
                }
            }, (error) => {
                this.utilService.dealWithRequestCatch(error, reject);
            })
        })
    }

}
