import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as Config } from '../../environments/environment';
import { Status } from '../models/status';
import { UtilService } from '../services/util.service';

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  constructor(
    private httpClient: HttpClient,
    private utilService: UtilService
  ) { }

  get() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/status`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  post(status: Status) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${Config.urlAPI}/status`, status, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  put(id: number, status: Status) {
    return new Promise((resolve, reject) => {
      this.httpClient.put(`${Config.urlAPI}/status/${id}`, status, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  delete(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(`${Config.urlAPI}/status/${id}`, this.utilService.headerPadrao()).subscribe(d => {
        resolve(d);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }
}
