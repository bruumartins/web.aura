import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Login } from 'src/app/models/login';
import { environment as Config } from '../../environments/environment';
import { User } from './../models/user';
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  httpOptions = {
    headers: new HttpHeaders({
      'responseType': 'json',
    }),
  }

  constructor(
    private httpClient: HttpClient,
    private utilService: UtilService
  ) { }


  authenticate(login: Login) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${Config.urlAPI}/auth`, login, this.utilService.headerPadrao()).subscribe(result => {
        resolve(result);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  post(user: User) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${Config.urlAPI}/user`, user, this.utilService.headerPadrao()).subscribe(result => {
        resolve(result);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  put(idUser: string, user: User) {
    return new Promise((resolve, reject) => {
      this.httpClient.put(`${Config.urlAPI}/user/${idUser}`, user, this.utilService.headerPadrao()).subscribe(result => {
        resolve(result);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  get() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/user`, this.utilService.headerPadrao()).subscribe(result => {
        resolve(result);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  delete(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(`${Config.urlAPI}/user/${id}`, this.utilService.headerPadrao()).subscribe(result => {
        resolve(result);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }

  getMyUser() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${Config.urlAPI}/user/getmyuser`, this.utilService.headerPadrao()).subscribe(result => {
        resolve(result);
      }, error => {
        this.utilService.dealWithRequestCatch(error, reject);
      });
    });
  }
}
