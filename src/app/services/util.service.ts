import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(
    private _snackBar: MatSnackBar,
    private router: Router
  ) { }

  headerPadrao() {
    return {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    }
  }

  isLogged() {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  dealWithRequestCatch(error: any, reject: any) {
    if (error.status && (error.status == 401 || error.status == 403)) {
      localStorage.removeItem('token');
      this.router.navigate(['/'], { replaceUrl: true });
    } else {
      reject(error);
    }
  }
}
